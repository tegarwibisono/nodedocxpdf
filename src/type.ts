export type payload = {
  url: string;
  parameter_tanggal_surat: string;
  parameter_nomor_surat: string;
  parameter_sifat_surat: string;
  parameter_perihal_surat: string;
  parameter_tte_nama: string;
  parameter_tte_pangkat: string;
  parameter_tte_nip: string;
  parameter_tte_jabatan: string;
  golongan: string;
};
