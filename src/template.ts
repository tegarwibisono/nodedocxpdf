import { createReport } from "docx-templates";
import qrcode from "easyqrcodejs-nodejs";
import fs from "fs";
import { getErrorMessage } from "./lib";

function splitFilename(fullFilename: string) {
  const filenameArr = fullFilename.split(".");
  const filename = filenameArr[0];
  const fileExtension = filenameArr[filenameArr.length - 1];
  return [filename, fileExtension];
}

export async function parseDocx(
  path: string,
  fileName: string,
  data: { url?: string; qrcode_size_mm?: string }
) {
  try {
    const originalFilePath = path + fileName;

    const template = fs.readFileSync(originalFilePath);
    const [originalFilename, fileExtension] = splitFilename(fileName);
    let count = 0;

    let url = data.url;
    // qrcode size string in mm
    let qrcode_sizeString = data.qrcode_size_mm ?? "20";
    // convert mm to cm
    let qrcode_size = parseInt(qrcode_sizeString) / 10;

    const buffer = await createReport({
      cmdDelimiter: ["${", "}"],
      template,
      additionalJsContext: {
        tteFunc: async function () {
          count++;
          if (!url || url === "dummy") {
            const data = fs.readFileSync("./assets/draft.png");
            return {
              width: qrcode_size,
              height: qrcode_size,
              data: data,
              extension: ".png",
            };
          }

          var qcode = new qrcode({
            text: url,
            logo: "./assets/logo.png",
          });

          const dataUrl = await qcode.toDataURL();
          const data = dataUrl.slice("data:image/png;base64,".length);

          return {
            width: qrcode_size,
            height: qrcode_size,
            data: data,
            extension: ".png",
          };
        },
      },

      data: data,
    });

    if (count === 0) {
      throw new Error("Tag ${TTE} tidak ditemukan");
    }

    const fileToWritePath =
      path + originalFilename + "-modified." + fileExtension;
    fs.writeFileSync(fileToWritePath, buffer);

    return fileToWritePath;
  } catch (e) {
    let splitted = getErrorMessage(e).split("command '");
    if (splitted.length > 1) {
      const msg = splitted[1];
      throw new Error(msg);
    }

    throw new Error(getErrorMessage(e));
  }
  return "";
}
