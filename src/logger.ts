import pino from "pino";
import * as dotenv from "dotenv";

dotenv.config();

const transport = pino.transport({
  targets: [
    {
      target: "pino/file",
      level: "debug",
      options: {},
    },
  ],
});

const pinoLogger = pino(transport);
export const logger = pinoLogger.child({
  app: "node-docx-pdf",
});
