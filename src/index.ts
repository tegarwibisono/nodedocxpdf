import { runServer } from "./server";
import * as dotenv from "dotenv";
import { logger } from "./logger";

dotenv.config();
try {
  logger.info("Starting server");
  runServer();
} catch (e: any) {
  logger.error(e);
}
