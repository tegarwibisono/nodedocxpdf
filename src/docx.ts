import fs from "fs";
import fetch from "node-fetch";
import FormData from "form-data";

// convert to pdf using rest api
export async function cleanupDocx(fileName: string) {
  const pdfConverterUrl = process.env.DOCX_CLEANER_URL;

  if (!pdfConverterUrl) {
    throw new Error("DOCX_CLEANER_URL is not defined");
  }

  let readStream = fs.createReadStream(fileName);
  const formData = new FormData();
  formData.append("file", readStream);

  const response = await fetch(pdfConverterUrl, {
    method: "POST",
    body: formData,
    timeout: 70000,
    headers: {
      Authorization: process.env.PDF_CONVERTER_TOKEN ?? "",
    },
  });

  if (!response.ok) {
    let data = await response.json();
    throw new Error("Failed to cleanup docx, " + data.detail);
  }

  return response;
}
