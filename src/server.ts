import express, { Express, Request, Response } from "express";
import multer from "multer";
import { parseDocx } from "./template";
import bodyParser from "body-parser";
import { payload } from "./type";
import { convertToPdf } from "./pdf";
import { cleanupDocx } from "./docx";
import fs from "fs";
import { getErrorMessage } from "./lib";
import { logger } from "./logger";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./tmp/");
  },
  filename: function (req, file, cb) {
    const filenameArr = file.originalname.split(".");
    const fileExtension = filenameArr[filenameArr.length - 1];

    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(null, uniqueSuffix + "." + fileExtension);
  },
});

const upload = multer({ storage: storage });

// middleware check authorization header
export function checkAuth(req: Request, res: Response, next: Function) {
  const authHeader = req.headers.authorization;
  if (!authHeader || !authHeader.startsWith("Bearer ")) {
    res.status(401).send({ error: "invalid authorization header" });
    return;
  }

  if (authHeader) {
    const token = authHeader.split(" ")[1];
    if (token === process.env.TOKEN) {
      next();
      return;
    }
    res.status(401).send({ error: "invalid authorization header" });
  }
}

export function runServer() {
  const app: Express = express();
  const port = process.env.PORT || 8000;
  app.use(bodyParser.urlencoded({ extended: true }));

  app.get("/", (req: Request, res: Response) => {
    res.send("Express + TypeScript Server");
  });

  // handle upload file from client and save it to tmp folder
  app.post(
    "/upload",
    checkAuth,
    upload.single("file"),
    async (req: Request<{}, {}, payload>, res: Response) => {
      const { file } = req;
      const loggerChild = logger.child({
        mime: file?.mimetype,
        fileName: file?.filename,
      });
      if (!file) {
        loggerChild.error("file is required");
        res.status(400).send({ error: "file is required" });
        return;
      }

      // if mime start with specific strings, then it is ms word file
      if (
        !file.mimetype.startsWith(
          "application/vnd.openxmlformats-officedocument.word"
        ) &&
        !file.mimetype.startsWith("application/msword")
      ) {
        loggerChild.error("file is not docx");
        res.status(400).send({ error: "file is not docx" });
        return;
      }

      // extract data from request body
      try {
        const t1 = Date.now();
        // cleanup docx file
        const cleanedPath = "./tmp2/";
        const fileStream = fs.createWriteStream(cleanedPath + file.filename);
        const resultDocx = await cleanupDocx(file.destination + file.filename);
        await new Promise((resolve, reject) => {
          resultDocx.body.pipe(fileStream);
          resultDocx.body.on("error", reject);
          fileStream.on("finish", resolve);
        });

        const timeToCleanupDocx = Date.now() - t1;

        // parse docx file template
        const t2 = Date.now();
        const docxPath = await parseDocx(cleanedPath, file.filename, {
          ...req.body,
        });
        const timeToGenerateDocx = Date.now() - t2;

        // convert docx to pdf
        const t3 = Date.now();
        const result = await convertToPdf(docxPath);
        const timeToConvertToPdf = Date.now() - t3;

        loggerChild.info(
          {
            timeToGenerateDocx: timeToGenerateDocx,
            timeToConvertToPdf: timeToConvertToPdf,
            timeToCleanupDocx: timeToCleanupDocx,
          },
          "success"
        );

        res.setHeader(
          "content-disposition",
          result.headers.get("content-disposition") ?? ""
        );
        res.setHeader("content-type", result.headers.get("content-type") ?? "");
        result.body.pipe(res);

        // delete cleaned file after convert to pdf
        fs.unlink(cleanedPath + file.filename, (err) => {
          if (err) {
            loggerChild.error(err.message);
          }
        });

        // delete file after convert to pdf
        fs.unlink(docxPath, (err) => {
          if (err) {
            loggerChild.error(err.message);
          }
        });
      } catch (e) {
        loggerChild.error(e);
        res.status(400).send({ error: getErrorMessage(e) });
      }

      fs.unlink(file.path, (err) => {
        if (err) {
          loggerChild.error(err.message);
        }
      });
    }
  );

  app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
  });
}
