FROM node:22 AS builder
RUN apt-get update \
    && apt-get install -y git ca-certificates \
    && apt-get install -y build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
COPY tsconfig.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM node:22 AS final
RUN apt-get update \
    && apt-get install -y git ca-certificates \
    && apt-get install -y build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
RUN npm install --production
COPY --from=builder /app/assets ./assets
COPY --from=builder /app/dist ./dist
RUN mkdir tmp && mkdir tmp2
CMD [ "npm", "start" ]